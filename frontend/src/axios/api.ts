import Axios from 'axios';

const api = Axios.create({ baseURL: 'http://localhost:5050/api/v1' });

export default api;