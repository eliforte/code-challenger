import React from 'react'
import ReactDOM from 'react-dom/client'
import { WallsContextProvider } from './context/wallsContext'
import {App }from './App'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <WallsContextProvider>
      <App />
    </WallsContextProvider>
  </React.StrictMode>
)
