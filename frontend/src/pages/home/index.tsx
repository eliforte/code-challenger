import React, { useEffect } from 'react';
import { AxiosError } from 'axios';
import { IInkCan } from '../../context/interface/IContext';
import Swal from 'sweetalert2'
import 'sweetalert2/src/sweetalert2.scss'
import api from '../../axios/api';
import { Wall } from '../../components/wall'
import { Square } from '../../components/square'
import { WallsContext } from '../../context/wallsContext'
import {
  DoItButton,
  MainContainer,
  HomeContainer,
  Form,
  Limits,
  TitleMessage,
  TotalWallsArea,
} from './styles';
import PaletteIcon from '@mui/icons-material/Palette';

export const Home: React.FC = () => {
  const { setCans, setLoading, walls } = React.useContext(WallsContext);
  const [disabled, setDisabled] = React.useState<boolean>(true);

  useEffect(() => {
    if (walls.length === 4) {
      setDisabled(false);
    }
  });
  
  const countSizesCans = (data: IInkCan[]) => {
    let message =  ['<ul>You will need the following cans:</ul>'];
    data.map((can) => {
      if (can.quantity > 1) {
        const text = `<li>${can.quantity} cans of ${can.size}L</li>`
        message.push(text)
      } else {
        const text = `<li>${can.quantity} can of ${can.size}L</li>`
        message.push(text)
      }
    })
    return message.join('<br>')
  }


  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      setLoading(true);
      const wallsInfosWithoutId = walls.map((wall) => {
        const { id, ...rest } = wall;
        return rest;
      })
      const { data } = await api.post('/room', {
        walls: wallsInfosWithoutId,
      });
      setCans(data.cans);
      setLoading(false);
      const result = countSizesCans(data.cans);
      Swal.fire({
        title: 'Done!',
        html: result,
        icon: 'success',
        confirmButtonText: 'OK',
      });
    } catch (err) {
      if (err instanceof AxiosError) {
        Swal.fire('Ops!', err.response?.data?.message, 'error');
        setLoading(false);
        throw err;

      } else {
        Swal.fire('Ops!', "Something it's wrong", 'error');
        setLoading(false);
      }
    }
  }

  return (
    <HomeContainer data-testid="home-container">
      <MainContainer>
        <TitleMessage data-testid="title-message" >
          Enter all wall measurements to receive the amount and capacity of paint cans to paint all walls.
        </TitleMessage>
        <Square />
        <Form data-testid="walls-form" onSubmit={ (event) => handleSubmit(event) }>
          <Limits data-testid="limits" >Total wall area: (min= 1m²| max= 50m²)</Limits>
          <TotalWallsArea data-testid="total-area">Total Area: { walls.reduce((acc, wall) => {
            const total = wall.width * wall.height;
            return acc + total;
          }, 0) }m²</TotalWallsArea>
          <Wall/>
          <DoItButton
            data-testid="doit-button"
            variant="contained"
            type='submit'
            disabled={ disabled }
            endIcon={<PaletteIcon />}
            sx={{ margin: '2rem 25%' }}
          >  
            Do it!
          </DoItButton>
        </Form>
      </MainContainer>
    </HomeContainer>
  );
}
