import styled from 'styled-components';
import { Button } from '@mui/material';

export const DoItButton = styled(Button)`
  width: 20%;
  align-self: center;
  margin: 0 auto;

  @media(max-width: 844px) {
    width: 50%;
  }
`
export const MainContainer = styled.main`
  display: flex;
  flex-direction: column;
  height: 100%;
  align-items: center;
`
export const HomeContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`
export const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  background-color: (253,245,230);
`
export const Limits = styled.h4`
  align-self: center;
`
export const TitleMessage = styled.h2`
  width: 60vw;
  margin: 10px auto;
  align-self: center;
  background: #F5F5F5;
  padding: 5px 10px;
  border-radius: 5px;

  @media(max-width: 844px) {
    width: 90%;
  }
`

export const TotalWallsArea = styled.span`
  align-self: center;
  margin: 10px auto;
  padding: 5px 10px;
  border-radius: 5px;
  background: #F5F5F5;
`
