export interface IWall {
  id: number;
  height: number;
  width: number;
  quantity_windows: number;
  quantity_doors: number;
}

export interface IInkCan {
  size: number;
  quantity: number;
}

export interface IWallsContext {
  walls: IWall[];
  setWalls: (newState: IWall[]) => void;
  loading: boolean;
  setLoading: (newState: boolean) => void;
  cans: IInkCan[];
  setCans: (newState: IInkCan[]) => void;
  identifier: number;
  setIdentifier: (newState: number) => void;
  show: boolean;
  setShow: (newState: boolean) => void;
  wallDetails: IWall;
  setWallDetails: (newState: IWall) => void;
  change: boolean;
  setChange: (newState: boolean) => void;
};



export type ChildrenProps = {
  children: React.ReactNode;
}