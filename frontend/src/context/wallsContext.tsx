import React, { createContext, useState, useEffect } from 'react';
import { IWallsContext, ChildrenProps, IWall, IInkCan } from './interface/IContext';

const initialState = {
  walls: [],
  setWalls: () => {},
  identifier: 1,
  setIdentifier: () => {},
  loading: false,
  setLoading: () => {},
  cans: [],
  setCans: () => {},
  show: true,
  setShow: () => {},
  wallDetails: {
    id: 0,
    height: 0,
    width: 0,
    quantity_doors: 0,
    quantity_windows: 0,
  },
  setWallDetails: () => {},
  change: false,
  setChange: () => {},
}

export const WallsContext = createContext<IWallsContext>(initialState);

export const WallsContextProvider = ({ children }: ChildrenProps) => {
  const [walls, setWalls] = useState<IWall[]>(initialState.walls);
  const [cans, setCans] = useState<IInkCan[]>(initialState.cans);
  const [loading, setLoading] = useState<boolean>(initialState.loading);
  const [identifier, setIdentifier] = useState<number>(initialState.identifier);
  const [wallDetails, setWallDetails] = useState<IWall>(initialState.wallDetails);
  const [change, setChange] = React.useState<boolean>(initialState.change);
  const [show, setShow] = useState<boolean>(initialState.show);

  useEffect(() => {
    if (walls.length === 4) {
      setShow(false);
    }
  }, [walls])

  return (
    <WallsContext.Provider
      value={{
        walls,
        setWalls,
        cans,
        setCans,
        loading,
        setLoading,
        identifier,
        setIdentifier,
        setShow,
        show,
        wallDetails,
        setWallDetails,
        setChange,
        change,
      }}>
      {children}
    </WallsContext.Provider>
  );
}