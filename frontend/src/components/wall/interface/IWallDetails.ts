export interface IWallDetails {
  id: number;
  height: number;
  width: number;
  quantity_doors: number;
  quantity_windows: number;
}
