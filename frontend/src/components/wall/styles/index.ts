import styled from 'styled-components';
import { TextField, Button } from '@mui/material';
import { IWallDetails } from '../interface/IWallDetails';

interface IDisplayProps {
  display: boolean;
  change: boolean;
}

interface IWallTotalAreaProps {
  totalArea: number;
}

export const InputContainer = styled.div<IDisplayProps>`
  display: ${props => props.display ? 'flex' : 'none'};
  flex-direction: column;
  background-color: #F5F5F5;
  width: 30%;
  border-radius: 5px;
  margin: 0 auto;
  padding: 10px;

  @media(max-width: 844px) {
    width: 70%;
  }

  @keyframes alertChange {
    0% {
      background-color: #DC143C;
      color: #F5F5F5;
    }
    50% {
      background-color: #FFC0CB;
      color: #F5F5F5;
    }
    100% {
      background-color: #F5F5F5;
      color: black;
    }
  }

  p {
    width: 10%;
    padding: 5px;
    border-radius: 5px;
    animation: ${props => props.change ? 'alertChange' : 'none'} 1s ease-in-out;
  }

  @media(max-width: 844px) {
    p { 
      width: 20%;
    }
  }

`

export const Input = styled(TextField)`
  margin: 20px 10px;
`
export const TotalAreaMessage = styled.span<IWallTotalAreaProps>`
  color: ${({ totalArea }) => totalArea > 0 && totalArea <= 50 ? 'black' : 'red'};
  margin: 10px auto 20px auto;
`
export const EditWalls = styled.span<IDisplayProps>`
  color: #4169E1;
  margin: 20px auto;
  cursor: pointer;
  text-decoration: underline;
  background-color: #F5F5F5;
  padding: 10px;
  border-radius: 5px;
  display: ${props => props.display ? 'none' : 'block'};
`

export const ButtonSave = styled(Button)`
  width: 50%;
  align-self: center;
`
export const WallsContainer = styled.div`
  display: flex;
  flex-direction: column;
`