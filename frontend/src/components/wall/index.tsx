import React from 'react';
import  { WallsContext } from '../../context/wallsContext';
import { IWallDetails } from './interface/IWallDetails';
import {
  InputContainer,
  Input,
  TotalAreaMessage,
  EditWalls,
  ButtonSave,
  WallsContainer,
} from './styles';

const initalState: IWallDetails = {
  id: 0,
  height: 0,
  width: 0,
  quantity_doors: 0,
  quantity_windows: 0,
}

export const Wall: React.FC = () => {
  const [isInvalid, setIsInvalid] = React.useState<boolean>(false)
  const [totalArea, setTotalArea] = React.useState<number>(0);
  const {
    identifier,
    wallDetails,
    setWallDetails,
    setIdentifier,
    walls, setWalls,
    setShow,
    show,
    change,
    setChange,
  } = React.useContext(WallsContext)

  
  const resultTotalArea = () => {
    const result = Math.round(wallDetails.height * wallDetails.width)
    setTotalArea(result)
  }

  React.useEffect(() => {
    resultTotalArea()
    if (wallDetails.height * wallDetails.width >= 1 && wallDetails.height * wallDetails.width <= 50) {
      setIsInvalid(false)
    } else {
      setIsInvalid(true)
    }
  }, [wallDetails])

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setWallDetails({ ...wallDetails, [name]: value, id: identifier })
  }

  const findWallInContext = () => {
    const wallsFiltered = walls.filter((wall) => wall.id === identifier)
    if (!wallsFiltered.length) {
      setWalls([...walls, wallDetails])
    } else {
      const updateInfoWall = { ...wallsFiltered[0], ...wallDetails }
      setWalls(walls.map((wall) => wall.id === identifier ? updateInfoWall : wall))
    }
  }

  const nextWall = () => {
    findWallInContext()
    if (identifier === 4) {
      setIdentifier(1)
      setWallDetails(initalState)
      setChange(true)
    } else {
      setIdentifier(identifier + 1)
      setWallDetails(initalState);
      setChange(true)
    }
  }

  return (
    <WallsContainer>
      <EditWalls
        data-testid="edit-walls"
        display={ show }
        change={ change }
        onClick={ () => setShow(true)}
      >
        Edit walls
      </EditWalls>
      <InputContainer
        data-testid="input-container"
        display={ show }
        change={ change }
      >
        <p data-testid="wall-identifier">Wall {identifier}</p>
        <Input
          data-testid="height-input"
          id='outlined-basic'
          value={wallDetails.height}
          label='Height'
          name='height'
          variant='outlined'
          type='number'
          size='small'
          margin='normal'
          onChange={handleChange}
        />
        <Input
          data-testid="width-input"
          id='outlined-basic'
          value={wallDetails.width}
          label='Width'
          name='width'
          variant='outlined'
          type='number'
          size='small'
          margin='normal'
          onChange={handleChange}
        />
        <Input
          data-testid="doors-input"
          id='outlined-basic'
          value={wallDetails.quantity_doors}
          label='Quantity Doors'
          name='quantity_doors'
          variant='outlined'
          type='number'
          size='small'
          margin='normal'
          onChange={handleChange}
        />
        <Input
          data-testid="windows-input"
          id='outlined-basic'
          value={wallDetails.quantity_windows}
          label='Quantity Windows'
          name='quantity_windows'
          variant='outlined'
          type='number'
          size='small'
          margin='normal'
          onChange={handleChange}
        />
        <TotalAreaMessage data-testid="total-wall-area" totalArea={ totalArea }>
          total wall area: { totalArea }m²
        </TotalAreaMessage>
        <ButtonSave
          data-testid="save-button"
          disabled={isInvalid}
          size='small'
          type='button'
          variant="contained"
          onClick={ () => nextWall() }
        >
          Save
        </ButtonSave>
      </InputContainer>
    </WallsContainer>
  )
}