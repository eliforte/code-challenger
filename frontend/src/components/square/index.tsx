import React from 'react';
import {
  LeftLine,
  TopLine,
  RightLine,
  BottomLine,
  SquareContainer,
  XContainer,
  ChooseMessage,
  Container,
} from './styles';
import { WallsContext } from '../../context/wallsContext';

export const Square: React.FC = () => {
  const { setIdentifier, walls, setWallDetails, setShow, setChange } = React.useContext(WallsContext);

  const handleClick = (id: number) => {
    if (walls.length) {
      walls.map((wall) => {
        if (wall.id === id) {
          setWallDetails(wall);
          setIdentifier(id);
        }
      })
    }
    setChange(true);
    setTimeout(() => {
      setChange(false);
    }, 300);
    setShow(true);
    setIdentifier(id);
  }

  return (
    <Container>
      <ChooseMessage data-testid="square-message"
      >
        Choose one of the walls to add information:
      </ChooseMessage>
      <SquareContainer>
        <TopLine
          data-testid="square-top-line"
          onClick={() => handleClick(2)}
        >
          <p>2</p>
        </TopLine>
          <XContainer>
            <LeftLine
              data-testid="square-left-line"
              onClick={() => handleClick(1)}
            >
              <p>1</p>
            </LeftLine>
            <RightLine
              data-testid="square-right-line"
              onClick={() => handleClick(3)}
            >
              <p>3</p>
            </RightLine>
          </XContainer>
        <BottomLine
          data-testid="square-bottom-line"
          onClick={() => handleClick(4)}
        >
          <p>4</p>
        </BottomLine>
      </SquareContainer>
    </Container>
  );
}