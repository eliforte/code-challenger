import styled from 'styled-components';

export const SquareContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 200px;
  margin: 0 auto;
  height: 150px;

  @media(max-width: 844px) {
    margin: 20px auto;
  }
`

export const XContainer = styled.div`
  display: flex;
`

export const LeftLine = styled.div`
  display: flex;
  background-color: #4169E1;
  width: 20px;
  height: 110px;
  margin-right: 160px;
  justify-content: center;
  align-items: center;

  :hover {
    border: 2px solid #ff4a4a;
    width: 16px;
    height: 106px;
  }

  p {
    color: white;
  }
`
export const TopLine = styled.div`
  background-color: #191970;
  width: 200px;
  height: 20px;
  color: white;
  text-align: center;

  :hover {
    border: 2px solid #d20000;
    width: 198px;
  }

  p {
    color: white;
    margin: 0;
  }
`
export const RightLine = styled(LeftLine)`
  margin-right: 0;
`
export const BottomLine = styled(TopLine)`
`
export const ChooseMessage = styled.span`
  margin: auto auto;
  font-size: 20px;
`
export const Container = styled.div`
  display: flex;
  flex-direction: row;
  width: 40%;
  height: 100%;
  margin: 40px auto;
  background: #F5F5F5;
  padding: 20px 20px;
  border-radius: 5px;

  @media(max-width: 844px) {
    flex-direction: column;
    position: relative;
    width: 85%;
  }
`