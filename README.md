# Digital Republic Challenge

***

## Sumário
- [Sobre](#sobre)
- [Contatos](#contatos)
- [Ferramentas](#ferramentas)
- [Iniciando projeto](#iniciando-projeto)
- [Scripts](#scripts)
- [Rodando com Docker](#rodando-com-docker)
- [Apresentação](#apresentacao)
- [Testes](#testes)
  - [Backend](#backend)
  - [Frontend](#frontend)
- [Rotas](#rotas)
  - [/api/v1/room](#api-v1-room)
- [Formatos de erros](#formatos-de-erros)
  - [Erros de validação](#erros-de-validacao)
  - [Regras da aplicação](#regras-da-aplicacao)
- [Feedbacks](#feedbacks)

***
## **Sobre**
  Aplicação desenvolvidor por [Elias Forte](https://github.com/eliforte) para o teste técnico da Digital Republic. O objetivo desta API é calcular a quantidade de tinta necessária para pintar uma sala dadas as medidas das 4 paredes e o número de portas e janelas, caso existam.
  
## **Contatos**
<a targer="_blank" href="https://www.instagram.com/eliifort/"><img src="https://img.icons8.com/fluency/48/000000/instagram-new.png"/></a>
<a targer="_blank" href="https://www.linkedin.com/in/elias-forte/"><img src="https://img.icons8.com/fluency/48/000000/linkedin.png"/></a>

***

## **Ferramentas**

- [Typescript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
- [Node.js](https://nodejs.org/en/docs/)
- [Express](https://expressjs.com/pt-br/)
- [Jest](https://jestjs.io/pt-BR/docs/getting-started)
- [SuperTest](https://github.com/visionmedia/supertest)

## **Iniciando projeto**

Primeiro use esse comando no seu terminal para fazer um clone do repositório no seu computador:

```bash
git clone git@gitlab.com:eliforte/code-challenger.git
```
Use o comando abaixo para entrar na pasta principal do projeto:

```bash
cd code-challenger
```

Na pasta raiz do projeto use:

```bash
yarn
```

## **Scripts**

- <code>yarn</code> => Para instalar todas as dependências do projeto.
- <code>yarn start</code> => Para iniciar a aplicação em ambiente de produção.
- <code>yarn dev</code> => Para iniciar a aplicação em ambiente de desenvolvimento.
- <code>yarn build</code> => Para iniciar o build a aplicação.
- <code>yarn test</code> => Para iniciar todos os testes.
- <code>yarn corverage</code> => Para ver a porcentagem de cobertura dos teste da aplicação.

***

## **Rodando com Docker**

Use os comandos abaixo no seu terminal:

Clone o repositório no seu computador.

```bash
git clone git@gitlab.com:eliforte/code-challenger.git
```
Entre na pasta principal do projeto.

```bash
cd code-challenger
```

Inicie os containers.

```bash
sudo docker-compose up
```

Após o comando o frontend da aplicação vai ficar disponível na url <code>http://localhost:3000/</code> e o backend na url <code>http://localhost:5050/</code>.

<hr>

## Apresentação

Video sobre o funcionamento do frontend da aplicação.

![Apresentação](https://gitlab.com/eliforte/code-challenger/-/raw/main/Frontend_apresentation.mp4)



<br>

## Testes
  ### Backend
  Para os testes do backend foram usados o Jest e o SuperTest.
  
  Cobertura total dos testes.
  ![Coberto dos testes](https://gitlab.com/eliforte/code-challenger/-/raw/main/assets/coveregeBackend.png)

## **Rotas**
  <br>

  ## <code>/api/v1/room</code><a name="api-v1-room"></a>
  
<br>

  Para conseguir a melhor combinação de tamanho e quantidade de latas de tintas, o usuário de fazer uma requisição do tipo **POST** para o endpoint <code>"/api/v1/room"</code>. 
  Todas as medidas (altura e largura) de todas as paredes devem ser informadas para conseguir um retorno positivo da API. Exemplo de corpo da requisição.

  ```json
  {
    "walls": [
        {
            "height": 3,
            "width": 3,
            "quantity_windows": 0,
            "quantity_doors": 1
        },
        {
            "height": 3,
            "width": 3,
            "quantity_windows": 1,
            "quantity_doors": 1
        },
        {
            "height": 3,
            "width": 3,
            "quantity_windows": 1,
            "quantity_doors": 1
        }
    ]
}

```


**Retorno da requição:**
```json
{
    "cans": [
      3.6,
      0.5
    ],
}
```
  **Formatos de erros que podem acontecer**
  - [Erros de validação](#erros-de-validacao)
  - [Regras da aplicação](#regras-da-aplicacao)
  
  <br>

O retorno acima significa que será preciso 1 lata de 3,6 litros e 1 de 0,5 litros. Então caso você receba como resposta:

```json
{
    "cans": [
      3.6,
      3.6,
      3.6,
      0.5,
      0.5,
      0.5,
    ],
}
```

O usuário precisará de 3 latas de 3,6 litros e 3 de 0,5 litros.

<hr>

## **Formatos de erros**

  ### <u>*Erros de validação*</u>

  São erros que ocorrem quando o cliente envia dados inválidos — em branco ou de tipo incorreto — gerando as respostas a seguir.


  - Campo altura da parede vazio:
  ```json
  {
    "message": "Please, enter wall height"
  }
  ```
  
  - Tipo incorreto:
  ```json
  {
    "message": "Wall height must be a number"
  }
  ```
  - Altura minima da parede:
  ```json
  {
    "message": "Wall height must be at least 2,20m"
  }
  ```

  Esses são alguns exemplos de erros que podem acontecer mas existe algumas outras varições que seguem o mesmo princípio e estrutura da messagens.

  ### <u>*Regras da aplicação*</u>

  São erros que ocorrem quando a API não conclui a ação devido a conflitos de informações recebidas ou infringe alguma regra.

   - Área da parede inválida:
  ```json
  {
    "message": "Wall area must be between 1m² and 50m²"
  }
  ```
  
  - Limite de área de portas e janelas excedidas:
  ```json
  {
    "message": "Doors and windows area has been exceeded. Limit 50% of wall area"
  }
  ```
