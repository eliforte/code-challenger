/* eslint-disable no-plusplus */
import { IWall } from '../utils/interface/IRoom';
import { ICanSize } from '../utils/interface/ICansSize';
import {
  WALL_AREA_INVALID,
  DOORS_AND_WINDOWS_AREA_EXCEEDED,
} from '../utils/errors';

export default class RoomService {
  private _walls: IWall[];

  private _windowArea: number;

  private _doorArea: number;

  private _totalWallAreaReceived: number;

  private _totalAreaToPaint: number;

  private _inkCans: number[];

  private _result: number[];

  constructor() {
    this._walls = [];
    this._inkCans = [];
    this._windowArea = 0;
    this._doorArea = 0;
    this._totalWallAreaReceived = 0;
    this._totalAreaToPaint = 0;
    this._result = [];
  }

  public set walls(walls: IWall[]) {
    this._walls = walls;
  }

  public set inkCans(inkCans: number[]) {
    this._inkCans = inkCans;
  }

  public set windowArea(windowArea: number) {
    this._windowArea = windowArea;
  }

  public set doorArea(doorArea: number) {
    this._doorArea = doorArea;
  }

  public minWallArea = (): void => {
    const result = this._walls.reduce((acc, wall) => {
      const total = wall.height * wall.width;
      if (total < 1 || total > 50) throw WALL_AREA_INVALID;
      return acc + total;
    }, 0);
    this._totalWallAreaReceived = result;
    this.AreaToPaint();
  };

  public AreaToPaint = (): void => {
    const result = this._walls.reduce((acc, wall) => {
      const total = (wall.quantity_windows * this._windowArea) + (
        wall.quantity_doors * this._doorArea
      );
      return acc + total;
    }, 0);

    if (result > this._totalWallAreaReceived * 0.5) throw DOORS_AND_WINDOWS_AREA_EXCEEDED;
    this._totalAreaToPaint = (this._totalWallAreaReceived - result) / 5;
  };

  public recommendedPaintCans = (): void => {
    this.minWallArea();
    let stop = this._totalAreaToPaint;
    const result = [];
    while (stop >= 0) {
      for (let i = 0; i < this._inkCans.length; i++) {
        if (stop >= this._inkCans[i]) {
          result.push(this._inkCans[i]);
          stop -= this._inkCans[i];
          break;
        }
      }
      if (stop < this._inkCans[this._inkCans.length - 1]) {
        result.push(this._inkCans[this._inkCans.length - 1]);
        stop -= this._inkCans[this._inkCans.length - 1];
      }
    }
    this._result = result;
  };

  public sizeOfPaintCans = (): ICanSize[] => {
    this.recommendedPaintCans();
    console.log(this._result);
    const allCansSize: ICanSize[] = [];
    this._result.forEach((canSize) => {
      if (!allCansSize.length) {
        allCansSize.push({ size: canSize, quantity: 1 });
      } else {
        const lastCanSize = allCansSize[allCansSize.length - 1];
        if (lastCanSize.size === canSize) {
          lastCanSize.quantity++;
        } else {
          allCansSize.push({ size: canSize, quantity: 1 });
        }
      }
    });
    return allCansSize;
  };
}
