import dotenv from 'dotenv';
import App from './app';
import RoomRoutes from '../routes/Room';
import ErrorHandler from '../middlewares/errors';

dotenv.config();

const app = new App();
const corsOptions = { origin: [`${process.env.PROD_CLIENT}`, 'http://localhost:3000'] };

app.useCors(corsOptions);
app.newRoutes(new RoomRoutes().router);
app.ErrorHandler(ErrorHandler.handler);

export default app;
