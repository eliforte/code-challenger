import { Router } from 'express';
import RoomController from '../controllers/Room';
import Validate from '../middlewares/validations/Validate';
import ValidateRoom from '../middlewares/validations/Room';

export default class Room {
  private _path: string = '/api/v1/room';

  private _router: Router;

  private _controller: RoomController;

  private _validade: Validate;

  constructor(
    controller = new RoomController(),
    validade: Validate = new ValidateRoom(),
  ) {
    this._controller = controller;
    this._validade = validade;
    this._router = Router();
    this._routes();
  }

  public get router(): Router {
    return this._router;
  }

  protected _routes = (): void => {
    this._router.post(this._path, this._validade.validateBodyRequest, this._controller.calculate);
  };
}
