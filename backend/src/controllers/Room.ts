import { Request as Req, Response as Res, NextFunction } from 'express';
import HttpStatusCodes from '../utils/enum/HttpStatusCodes';
import RoomService from '../services/Room';

export default class RoomController {
  protected _service: RoomService;

  constructor(service: RoomService = new RoomService()) {
    this._service = service;
  }

  public calculate = async (req: Req, res: Res, next: NextFunction):
  Promise<typeof res | void> => {
    try {
      const { walls } = req.body;
      this._service.inkCans = [18, 3.6, 2.5, 0.5];
      this._service.windowArea = 2.4;
      this._service.doorArea = 1.52;
      this._service.walls = walls;
      const result = this._service.sizeOfPaintCans();
      return res.status(HttpStatusCodes.OK).json({ cans: result });
    } catch (error) {
      return next(error);
    }
  };
}
