import joi from 'joi';

const roomSchema = joi.object({
  walls: joi.array().items(joi.object({
    height: joi.number().min(2.2).required()
      .messages({
        'number.base': 'Wall height must be a number',
        'number.min': 'Wall height must be at least 2,20m',
        'any.required': 'Please, enter wall height',
      }),
    width: joi.number().required()
      .messages({
        'number.base': 'Wall width must be a number',
        'any.required': 'Please, enter wall width',
      }),
    quantity_windows: joi.number().min(0).required()
      .messages({
        'number.base': 'Quantity of windows must be a number',
        'any.required': 'Please, enter quantity of windows',
      }),
    quantity_doors: joi.number().min(0).required()
      .messages({
        'number.base': 'Quantity of doors must be a number',
        'any.required': 'Please, enter quantity of doors',
      }),
  })).length(4).required()
    .messages({
      'array.length': 'Please, enter all walls size',
    }),
});

export default roomSchema;
