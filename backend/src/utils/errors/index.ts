import NewError from './NewError';
import HttpStatusCodes from '../enum/HttpStatusCodes';

export const WALL_AREA_INVALID = new NewError(HttpStatusCodes.BAD_REQUEST, 'Wall area must be between 1m² and 50m²');

export const DOORS_AND_WINDOWS_AREA_EXCEEDED = new NewError(HttpStatusCodes.BAD_REQUEST, 'Doors and windows area has been exceeded. Limit 50% of wall area');
