export interface ICanSize {
  size: number;
  quantity: number;
}
