export interface IWall {
  height: number;
  width: number;
  quantity_windows: number;
  quantity_doors: number;
}

export interface IRoom {
  walls: IWall[];
}
