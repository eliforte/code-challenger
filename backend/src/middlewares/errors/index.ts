/* eslint-disable no-unused-vars */
import { NextFunction as Next, Request as Req, Response as Res } from 'express';
import { ValidationError } from 'joi';
import HttpStatusCodes from '../../utils/enum/HttpStatusCodes';
import { IError } from '../../utils/interface/IError';

export default class ErrorHandler {
  public static handler = (error: IError & ValidationError, req: Req, res: Res, _next: Next) => {
    if (error.isJoi) {
      return res.status(HttpStatusCodes.BAD_REQUEST).json({ message: error.details[0].message });
    }

    if (error.status) {
      return res.status(error.status).json({ message: error.message });
    }

    return res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: error.message });
  };
}
