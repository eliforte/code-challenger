import { ObjectSchema } from 'joi';
import Validate from './Validate';
import roomSchema from '../../utils/schemas';

export default class ValidateRoom extends Validate {
  constructor(schema: ObjectSchema = roomSchema) {
    super(schema);
  }
}
