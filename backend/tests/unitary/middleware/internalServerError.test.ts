import request from 'supertest';
import app from '../../../src/api';
import { correctRequestBody } from '../../end2End/room/mocks';

const myApp = app.server(0);

jest.mock('../../../src/services/Room');

describe('Test internal server error', () => {
  it('should return internal server error', async () => {
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(correctRequestBody);

    expect(response.status).toBe(500);
    expect(response.body).toHaveProperty('message');
  });
});