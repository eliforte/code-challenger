import request from 'supertest';
import app from '../../../src/api';
import { wrongSizeRequestBody, correctRequestBody } from '../../end2End/room/mocks';

const myApp = app.server(0);

describe('Test internal server error', () => {
  it('should return internal server error', async () => {
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(wrongSizeRequestBody);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
  });

  it('door and window area must be lass than 50% total wall area', async () => {
    correctRequestBody.walls[0].quantity_doors = 15;
    correctRequestBody.walls[0].quantity_windows = 15;

    const response = await request(myApp)
      .post('/api/v1/room')
      .send(correctRequestBody);
      
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Doors and windows area has been exceeded. Limit 50% of wall area');
  });
});