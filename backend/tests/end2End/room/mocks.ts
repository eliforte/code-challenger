export const correctRequestBody = {
  walls: [
    {
      height: 4,
      width: 4,
      quantity_windows: 0,
      quantity_doors: 0,
    },
    {
      height: 4,
      width: 4,
      quantity_windows: 0,
      quantity_doors: 0,
    },
    {
      height: 4,
      width: 4,
      quantity_windows: 0,
      quantity_doors: 0,
    },
    {
      height: 4,
      width: 4,
      quantity_windows: 0,
      quantity_doors: 0,
    },
  ],
};

export const successResponseBody = [{"quantity": 3, "size": 3.6}, {"quantity": 5, "size": 0.5}]

export const wrongSizeRequestBody = {
  walls: [
    {
      height: 4,
      width: 4,
      quantity_windows: 0,
      quantity_doors: 0,
    },
  ],
};
