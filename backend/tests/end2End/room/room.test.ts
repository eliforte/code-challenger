import request from 'supertest';
import app from '../../../src/api';
import {
  correctRequestBody,
  wrongSizeRequestBody,
  successResponseBody,
} from './mocks';

const myApp = app.server(0);

const changeValuesIntoMock = (key: string, value: string | number | undefined) => {
  const mock = JSON.parse(JSON.stringify(correctRequestBody));
  mock.walls[0][key] = value;
  return mock;
}

describe('Test route /room', () => {
  it('should post information incomplet', async () => {
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(wrongSizeRequestBody);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Please, enter all walls size');
  });

  it('height of the wall must be a number', async () => {
    const heightIsNotNumber = changeValuesIntoMock('height', 'teste');
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(heightIsNotNumber);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Wall height must be a number');
  });

  it('height must be a grander than 2,20m', async () => {
    const heightIsNotNumber = changeValuesIntoMock('height', 1);
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(heightIsNotNumber);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Wall height must be at least 2,20m');
  });

  it('height its required', async () => {
    const heightIsNotNumber = changeValuesIntoMock('height', undefined);
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(heightIsNotNumber);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Please, enter wall height');
  })

  it('width its required', async () => {
    const widthIsNotNumber = changeValuesIntoMock('width', undefined);
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(widthIsNotNumber);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Please, enter wall width');
  });

  it('width of the wall must be a number', async () => {
    const widthIsNotNumber = changeValuesIntoMock('width', 'teste');
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(widthIsNotNumber);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Wall width must be a number');
  });

  it('quantity of windows must be a number', async () => {
    const quantityOfWindowsIsNotNumber = changeValuesIntoMock('quantity_windows', 'teste');
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(quantityOfWindowsIsNotNumber);
    
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Quantity of windows must be a number');
  });

  it('quantity of windows its required', async () => {
    const quantityOfWindowsIsNotNumber = changeValuesIntoMock('quantity_windows', undefined);
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(quantityOfWindowsIsNotNumber);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Please, enter quantity of windows');
  });

  it('quantity of doors must be a number', async () => {
    const quantityOfDoorsIsNotNumber = changeValuesIntoMock('quantity_doors', 'teste');
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(quantityOfDoorsIsNotNumber);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Quantity of doors must be a number');
  });

  it('quantity of doors its required', async () => {
    const quantityOfDoorsIsNotNumber = changeValuesIntoMock('quantity_doors', undefined);
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(quantityOfDoorsIsNotNumber);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Please, enter quantity of doors');
  });

  it('wall area must be between 1m² and 50m²', async () => {
    const wallAreaIsNotBetween = changeValuesIntoMock('height', 25);
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(wallAreaIsNotBetween);

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Wall area must be between 1m² and 50m²');
  });

  it('door and window area must be lass than 50% total wall area', async () => {
    const doorAndWindowAreaIsGreaterThan = changeValuesIntoMock('height', 10);
    doorAndWindowAreaIsGreaterThan.walls[0].quantity_doors = 15;
    doorAndWindowAreaIsGreaterThan.walls[0].quantity_windows = 15;

    const response = await request(myApp)
      .post('/api/v1/room')
      .send(doorAndWindowAreaIsGreaterThan);
      
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Doors and windows area has been exceeded. Limit 50% of wall area');
  });

  it('should post information complet', async () => {
    const response = await request(myApp)
      .post('/api/v1/room')
      .send(correctRequestBody);

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('cans');
    expect(response.body.cans).toStrictEqual(successResponseBody);
  });
});
